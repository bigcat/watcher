/*! string.format.js */
!function(t){function n(t){return{}.toString.call(t).slice(8,-1)}function r(t){return"Function"===n(t)}function i(n,r){if(n===t)return t;if(r.length){var l=r.shift();return n[l]===t?t:i(n[l],r)}return n}String.prototype.format=function(){var l=[].slice.call(arguments,0);return l.length?this.replace(/\{(.+?)\}/g,function(){var e=arguments[0],o=/^\{(#?)([\d\_\.a-z]+?)(\((.*?)\))?\s*(\[([\d\_a-z]+)\])?(\|(.+))?\}$/i.exec(e),p=o[1],s=o[2],u=o[4],a=o[6],f=o[8],g=t;if(g=p&&/\d+/.test(s)?l[s]:i(l[0],s.split(/\s*?\.\s*?/)),r(g)&&u&&(g=g.apply(null,u.split(/\s*?\,\s*?/))),a&&(g=i(g,a.split(/\s*?\.\s*?/))),g===t)return e;if(f){f=f.split(/\s*?\|\s*?/);for(var c=0,y=f.length;y>c;c++){for(var S=f[c].split(/\s*?\:\s*?/),h=S.shift(),m=null,y=l.length,d=l[0];d=l[c++];)"Object"==n(d)&&(m=r(d[h])?d[h]:m);r(m)?g=m.apply(null,[g].concat(S)):(m=String.prototype.format_modifiers[h],r(m)?g=m.apply(g,S):(m=String.prototype[h],"String"==n(g)&&r(m)?g=m.apply(g.toString(),S):0+g==g&&(g=+g,m=Number.prototype[h],r(m))?g=m.apply(g,S).toString():(m=window[h],r(m)&&(S.unshift(g),g=m.apply(null,S)))))}}return(g===t?e:null===g?"null":g).toString()}):String(this)},String.prototype.format_modifiers={}}();

!(function($){
	String.prototype.truncate = function(length, truncation){
	    length = length || 30;
	    truncation = truncation==null?'...':truncation;
	    return this.length > length ?
	        this.slice(0, length - Math.ceil(truncation.length/3))+truncation :
	        this.toString();
	};
	String.prototype.initial = function(){
		if(!this.toString()) return "";
		return this.substr(0,1).toUpperCase()+this.substr(1).toLowerCase();
	}
	// 回车事件
	$.fn.enter = function (fn) {
		return this.keydown(function (e) {
			if (e.which == 13) {
				(fn||$.k).call(this, e);
			}
		});
	};

	var sp = window.Inspector = function(){ return this.init.apply(this, arguments) };

	sp.prototype = {
		$log_list:null,
		$filter:null,
		socket:null,
		thread_end_queue:[],
		init:function(socket){
			var that = this;
			this.socket = socket;
			var $list = this.$log_list = $("#log_list");
			this.$filter = $("#filter");
			this.$filter.find("input[name=fkw]").enter(function(e){
				var val = this.value;
				that.subFilter( val );
				this.value = "";
			});

			this.$filter.find("button").click(function(){
				that.clear();
			})

			$list.on("click", ".thread-title", function(){
				$(this).parent().toggleClass("expand");
			});
		},
		push:function(data){
			switch(data.type.toLowerCase()){
				case "threadstart":
					return this.renderThead(data);
				case "threadend":
					return this.threadEnd(data);
				default:
					return this.renderTrace(data);
			}
		},
		renderThead:function(th){
			var $convert = $("<div/>");
			th.requestURI = this.htmlEntity(th.requestURI);
			var html = [
			'<dl id="{thread}" class="thread" thread="{thread}">',
				'<dt class=thread-title><i class="icon-prefix"></i><span class=uri title="{host}{requestURI}">{httpMethod|toUpperCase()} {host}{requestURI}</span> <span class=duration></span></dt>',
				'<dd class=params><sub></sub><div class=param-list></div><sup></sup></dd>',
				'<dd class=thread-subs></dd>',
			'</dl>'
			].join('').format(th);
			$convert.html(html).find(".param-list").html(this.parseParams(th));
			
			this.appendToThread(th.thread, $convert.html() );
		},
		threadEnd:function(th){
			this.thread_end_queue.push(th);
			clearTimeout(this._tick_tm);
			this._tickThreadEnd();
		},
		_tick_tm:null,
		_tickThreadEnd:function(){
			var s = this;
			var $t, thread;
			for(var i=0,l=this.thread_end_queue.length;i<l; i++){
				thread = this.thread_end_queue[i];
				$t = $("#"+thread.thread);
				if($t.size()){
					$t.addClass("complete").find(".duration").eq(0).html((thread.duration*1000).toFixed(3) + " ms");
					this.thread_end_queue.splice(i,1);
					i--;
					l--;
				}
			}
			s._tick_tm = setTimeout(function(){
				s._tickThreadEnd();
			},50);
		},

		parseParams:function(data){
			var s = this;
			var type = ({}).toString.apply(data);
			switch(type){
				case "[object String]":
					return '<span class=string>'+s.htmlEntity(data)+'</span>';
				case "[object Number]":
					return '<span class=number>'+data+'</span>';
				case "[object Null]":
					return '<span class=null>null</span>';

			}
			var html = [];
			$.each(data,function(k,v){
				html.push('<dl class=param><dt>{#0}</dt><dl>{#1}</dl></dl>'.format(k,s.parseParams(v)) );
			});
			return html.join('');
		},
		htmlEntity:function(str){
			return $("<div></div>").text(str).html()
		},

		appendToThread:function(thread, html){
			var threads = thread.split("_");
			var master = true;
			for(var $thread, l=threads.length; l ; l--){
				$thread = $( "#"+threads.slice(0,l).join("_"));
				if( $thread.size() ){
					$thread.find(".thread-subs").eq(0).append(html);
					master = false;
					return true;
				}
			}
			this.$log_list.append(html);
		},



		renderTrace:function(log){
			var html = [
			'<dl class="log log-{type}" type={type}>',
				'<dt>',
					'<label>{type|initial}: </label>',
					(log.type=="log"?"{parsedArgs}" :'<span class=message>{fire.message}</span>'),
				'</dt>',
				'<dt class=file>{fire.file}<i>(第{fire.line}行)</i></dt>',
			'</dl>'
			].join('').format(log).format({parsedArgs:this.argsToStr(log.fire.args)});

			this.appendToThread(log.thread, html);
		},
		argsToStr:function(args){
			if(!args) return "";
			var args =  $.map(args,function(arg,i){
				var type = ({}).toString.apply(arg);
				switch(type){
					case "[object String]":
						return '<span class=string>'+$("<div></div>").text(arg).html()+'</span>';
					case "[object Number]":
						return '<span class=number>'+arg+'</span>';
					default:
						return '<span class=object>'+type+'</span>';
				}
			});
			return args.join(", ");
		},
		clear:function(){
			this.$log_list.html("");
		},
		subFilter:function(filter){
			this.socket.emit("filter", filter );
		},
		updateFilter:function(data){
			console.log(data);
		}
	};
})(jQuery)